FROM rocker/shiny:3.6.0

RUN apt-get update && apt-get install libcurl4-openssl-dev libv8-3.14-dev libssl-dev -y &&\
mkdir -p /var/lib/shiny-server/bookmarks/shiny


RUN R -e "install.packages(c('shinydashboard', 'shinyjs', 'shiny', 'shinythemes', 'magrittr', 
                             'stringr', 'vegan', 'reshape2', 'dplyr', 'igraph', 'devtools', 
                             'data.table', 'DT'))"

RUN R -e "devtools::install_github('zdk123/SpiecEasi')"

RUN chmod -R 755 /srv/shiny-server/

EXPOSE 3838

CMD ["/usr/bin/shiny-server.sh"]
