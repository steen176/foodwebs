## Hi there, welcome

We will start by uploading your data in the web-app. If you have the table in excel, you need to export it to a CSV file (in Excel: file -> export -> change file type -> CSV).
After saving you can upload the file here by clicking `Browse`. The data will then be displayed below. If it looks like a normal table you are good to go. If the columns do not display correctly, you might want to change the separator. 

**Note:** *if you have a lot of columns in your table then it likely does not fit on the screen, that is fine.*

Then before we dive into the analysis, we have to select all columns that are **not** biotic factors (e.g. IDs, Treatment variable, pH, etc.). You can do that by just clicking on a value in that column (the column will change color if you selected it).