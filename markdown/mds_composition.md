
#### Community composition
In the NMDS plot, each dot represents a sample, and the closer the samples
appear in the NMDS graph, the more similar they are. Following the same logic, 
the more distant the points are, the more dissimilar also the sample are.

In the NMDS plot it is possible to show the effect of treatment on your communities/samples by drawing 
convex hulls connecting the vertices of the points made for the samples on the plot. 
This allows to better understand how communities/samples and species cluster based on treatments.

In NMDS plots is also possible to fit environmental variables, such as pH and moisture, 
in order to relate community data to environmental factors. 
Environmental variables are plotted on the NMDS graphs as arrows.
The arrow(s) point to the direction of most rapid change in the environmental variable. 
Often this is called the direction of the gradient.
The length of the arrow(s) is proportional to the correlation between ordination of your samples on your 2D space 
and environmental variable. Often this is called the strength of the gradient.
