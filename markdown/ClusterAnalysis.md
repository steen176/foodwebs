## Cluster Analysis

Another common way of visualising similarity between samples is through clustering. Here, a dendogram is shown that shows the hierachical relation between the samples. Using this method, samples are separated from each other using branches, where the length of each branch determines the similarity (short means similar, long means more distant).